import { NgModule } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule, MatButtonModule, MatChipsModule, MatDialogModule } from '@angular/material';
import { MatProgressSpinnerModule, MatTooltipModule, MatTabsModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';

@NgModule({
  exports: [
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
    MatDialogModule
  ],
  // providers: [{ provide: MAT_DATE_LOCALE, useValue: 'vi-VI' }],
  // providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-US' }],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'ja-JP' }]
})
export class MatComponentsModule {}
