import { Component } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { MatDialog } from '@angular/material';
import { EditCourseComponent } from './edit-course/edit-course.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // determinate spinner 用
  progress = 0;
  timer;
  // indeterminate spinner 用
  isLoading = false;

  categories = [{ name: 'Beginner' }, { name: 'Intermediate' }, { name: 'Advanced' }];

  minDate = new Date(2018, 0, 1); // jan 1st
  maxDate = new Date(2018, 7, 1); // august 1st

  colors = [{ id: 1, name: 'Red' }, { id: 2, name: 'Green' }, { id: 3, name: 'Blue' }];

  color = 2; // set default to green

  title = 'material-demo';
  isChecked = true;

  constructor(private dialog: MatDialog) {
    // determinate spinner 用
    this.timer = setInterval(() => {
      this.progress++;
      if (this.progress === 100) clearInterval(this.timer);
    }, 20);

    // indeterminate spinner 用
    this.isLoading = true;

    this.getCourses().subscribe(x => (this.isLoading = false));
  }

  openDialog() {
    this.dialog
      .open(EditCourseComponent, {
        data: { courseId: 1 }
      })
      .afterClosed()
      .subscribe(result => console.log(result));
  }

  getCourses() {
    // return an observable that will emit a value after a given time (simulate a call to the server that will take 2 seconds)
    return timer(5000);
  }

  onChange($event) {
    console.log($event);
  }

  selectCategory(category) {
    this.categories.filter(c => c !== category).forEach(c => (c['selected'] = false));

    category.selected = !category.selected;
  }
}
